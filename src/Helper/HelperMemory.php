<?php

declare(strict_types=1);

namespace Tektine\Bundle\SchedulerBundle\Helper;

use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

/**
 * Helper dédié au monotoring de la mémoire.
 * Ne gère pas les memory_limit à -1.
 */
class HelperMemory
{
    /**
     * Retourne la limite mémoire
     */
    public function getMemoryLimit(): int
    {
        $value   = trim($this->getRawMemoryLimitParameter());
        $matches = array();
        preg_match('/([0-9]+)([A-Z]+)?/i', $value, $matches);

        $value = (integer) $matches[1];

        if (isset($matches[2])) {
            $last = strtolower($matches[2]);
            switch($last) {
                case 'g':
                case 'gb':
                    $value *= 1024;
                case 'm':
                case 'mb':
                    $value *= 1024;
                case 'k':
                case 'kb':
                    $value *= 1024;
            }
        }

        return $value;
    }

    /**
     * Retourne le paramètre de memory_limit
     * 
     * @return string
     */
    public function getRawMemoryLimitParameter()
    {
        return ini_get('memory_limit');
    }

    /**
     * Retourne la consommation mémoire actuelle en MB
     * 
     * @return number
     */
    public function getCurrentConsumptionInMB()
    {
        return $this->getCurrentConsumptionInKB() / 1024;
    }

    /**
     * Retourne la consommation actuelle en KB
     * 
     * @return number
     */
    public function getCurrentConsumptionInKB()
    {
        return $this->getCurrentConsumption() / 1024;
    }

    /**
     * Retourne la consommation mémoire actuelle
     * 
     * @return number
     */
    public function getCurrentConsumption()
    {
        return memory_get_usage();
    }

    /**
     * Retourne le pic de consommation mémoire
     * 
     * @return number
     */
    public function getPeakConsumption()
    {
        return memory_get_peak_usage();
    }

    /**
     * Retourne le pic de consommation mémoire en KB
     * 
     * @return number
     */
    public function getPeakConsumptionInKB()
    {
        return $this->getPeakConsumption() / 1024;
    }

    /**
     * Retourne le pic de consommation mémoire en MB
     * 
     * @return number
     */
    public function getPeakConsumptionInMB()
    {
        return $this->getPeakConsumptionInKB() / 1024;
    }

    /**
     * Retourner le message de log avec les infos de mémoire et le logger directement si un Log_Base est passé en arguement
     *
     * @param string      $message
     * @param LoggerInterface|null $logger
     * @param integer     $priority
     *
     * @return string
     */
    public function logActionWithMemoryInfo($message, $logger = null, $priority = LogLevel::INFO)
    {
        gc_collect_cycles();
        $memMB     = $this->getCurrentConsumptionInMB();
        $memPeakMB = $this->getPeakConsumptionInMB();
        $result    = sprintf('%gMio' . "\t" . '| %gMio Peak' . "\t| %s", $memMB, $memPeakMB, $message);

        if ($logger instanceof LoggerInterface) {
            $logger->log($priority,$result);
        }

        return $result;
    }
}