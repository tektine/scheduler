<?php

declare(strict_types=1);

namespace Tektine\Bundle\SchedulerBundle\Helper;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Part\TextPart;

/**
 * Class MailHelper
 *
 * - permet la construction de mails simples avec un body dans un MailTemplate
 * - permet la constructions de mails à partir de templates twig
 * - enregistre le mail pour historisation
 *
 * @package NotificationBundle\Helper
 */
class MailHelper implements IMailHelper
{
    /** @var MailerInterface $mailer */
    private $mailer;

    /** @var TemplatedEmail $message */
    private $message;
    private $to;
    private $fromEmail;
    private $fromName;

    /**
     * MailHelper constructor.
     *
     * @param MailerInterface $mailer
     */
	public function __construct(MailerInterface $mailer, $to, $fromName, $fromEmail)
    {
        $this->mailer = $mailer;
        $this->to = $to;
        $this->fromEmail = $fromEmail;
        $this->fromName = $fromName;
    }



    public function send($body, $subject, $to = null, $fromName = null, $fromEmail = null)
    {
        $this->message = (new TemplatedEmail());
        $this->message->setBody(new TextPart('test'));
        $this->message->subject($subject);
        $to = $to?:$this->to;
        $fromEmail = $fromEmail?:$this->fromEmail;
        $fromName = $fromName?:$this->fromName;
        $this->message->to(new Address($to));
        $this->message->from(new Address($fromEmail, $fromName));
        try {
            $this->mailer->send($this->message);
        } catch (\Exception $e) {
            // do nothing
        }
    }

    /**
     * @param string $text
     */
    protected function setBody($text)
    {
        $this->message->setBody(new TextPart($text));
    }

}