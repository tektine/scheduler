<?php

declare(strict_types=1);

namespace Tektine\Bundle\SchedulerBundle\Helper;

interface IMailHelper
{
	public function send(string $body, string $subject, $to = null, $fromEmail = null);
}