<?php

declare(strict_types=1);

namespace Tektine\Bundle\SchedulerBundle\Service;

use DateTime;
use Exception;
use InvalidArgumentException;

class NextRunComputer
{
	/**
	 * @param $cron
	 * @param DateTime $fromDate
	 * @param null $nextDate
	 * @return DateTime
	 * @throws Exception
	 */
	public function computeNextRun($cron, $fromDate = null, $nextDate = null): ?DateTime
    {
		// Search next datetime
		if (!$nextDate) {
			$nextDate = new DateTime();
			$nextDate->setTime((int) $nextDate->format('H'), (int)$nextDate->format('i') + 1);
		}
		if ($fromDate && $fromDate > $nextDate) {
			$nextDate = clone($fromDate);
		}

		// Parse cron and check good values
		$crons = explode(' ', $cron);
		if (count($crons) !== 5) {
			throw new InvalidArgumentException(sprintf('Parse error %s', $cron));
		}
		try {
			$minutes  = $this->parsePossibleValues($crons[0], 0, 59);
			$hours    = $this->parsePossibleValues($crons[1], 0, 23);
			$days     = $this->parsePossibleValues($crons[2], 1, 31);
			$months   = $this->parsePossibleValues($crons[3], 1, 12);
			$weekdays = $this->parsePossibleValues($crons[4], 0, 7);
		} catch (InvalidArgumentException $e) {
			throw new InvalidArgumentException(sprintf('Parse error %s', $cron));
		}

		// Add 7 value for sunday
		if (in_array(7, $weekdays) && !in_array(0, $weekdays)) {
			$weekdays[] = 0;
		}

		while (!in_array($nextDate->format('m'), $months)
			|| !in_array($nextDate->format('d'), $days)
			|| !in_array($nextDate->format('H'), $hours)
			|| !in_array($nextDate->format('i'), $minutes)
			|| !in_array($nextDate->format('w'), $weekdays)
		) {
			if (!in_array($nextDate->format('m'), $months)) {
				$nextDate->setDate((int) $nextDate->format('Y'), (int)$nextDate->format('m') + 1, 1);
				$nextDate->setTime(0, 0, 0);
			} elseif (!in_array($nextDate->format('d'), $days) || !in_array($nextDate->format('w'), $weekdays)) {
				$nextDate->modify('+1 day');
				$nextDate->setTime(0, 0, 0);
			} elseif (!in_array($nextDate->format('H'), $hours)) {
				$nextDate->setTime((int)$nextDate->format('H') + 1, 0, 0);
			} elseif (!in_array($nextDate->format('i'), $minutes)) {
				$nextDate->modify('+1 minute');
			}
			if ($nextDate->format('Y') > date('Y') + 10) {
				throw new InvalidArgumentException(sprintf('Infinite loop for %s and %s > %s', $cron, $fromDate ? $fromDate->format('Y-m-d H:i:s') : 'null', $nextDate->format('Y-m-d H:i:s')));
			}
		}

		return $nextDate;
	}


	/**
	 * @param string $cron
	 * @param int $minRange
	 * @param int $maxRange
	 * @return array
	 */
	protected function parsePossibleValues($cron, $minRange = 0, $maxRange = 60): array
    {
		$result = [];
		$tab = explode(',', $cron);
		foreach($tab as $row) {
			$range = $row;
			$step = 1;
			if (strpos($row, '/') !== false) {
				list($range, $step) = explode('/', $row);
				if (!is_numeric($step)) {
					throw new InvalidArgumentException(sprintf('Parse error %s', $cron));
				}
			}
			if ($range === '*') {
				$result = array_merge($result, range($minRange, $maxRange, $step));
			} elseif (strpos($range, '-') !== false) {
				$tabRange = explode('-', $range);
				if (!is_numeric($tabRange[0])
					|| !is_numeric($tabRange[1])
					|| $tabRange[0] > $tabRange[1]
					|| $tabRange[0] < $minRange
					|| $tabRange[1] > $maxRange) {
					throw new InvalidArgumentException(sprintf('Parse error %s', $cron));
				}
				$result = array_merge($result, range($tabRange[0], $tabRange[1], $step));
			} elseif (is_numeric($range) && $range >= $minRange && $range <= $maxRange) {
				$result[] = $range;
			} else {
				throw new InvalidArgumentException(sprintf('Parse error %s', $cron));
			}
		}

		return array_unique($result);
	}
}