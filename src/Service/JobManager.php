<?php


namespace Tektine\Bundle\SchedulerBundle\Service;


use Tektine\Bundle\SchedulerBundle\Job\JobBase;

class JobManager
{
    /** @var JobBase[] */
    private $jobs = [];

    public function addJob(JobBase $job)
    {
        $this->jobs[] = $job;
    }

    public function getJob(string $className): ?JobBase
    {
        foreach ($this->jobs as $job) {
            if ($job instanceof $className) {
                return $job;
            }
        }
        return null;
    }
}