<?php

declare(strict_types=1);

namespace Tektine\Bundle\SchedulerBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Component\Console\Command\Command;
use Tektine\Bundle\SchedulerBundle\Helper\IMailHelper;
use Tektine\Bundle\SchedulerBundle\Repository\SchedulerRepository;
use Tektine\Bundle\SchedulerBundle\Service\JobManager;
use Tektine\Bundle\SchedulerBundle\Service\NextRunComputer;
use Tektine\Bundle\SchedulerBundle\Entity\Scheduler;
use Tektine\Bundle\SchedulerBundle\Job\JobBase;
use Tektine\Bundle\SchedulerBundle\Model\IGlobalParameter;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class AppCronCommand
 * A command console that launch application cron process.
 *
 * To use this command, open a terminal window, enter into your project
 * directory and execute the following:
 *
 *     $ php bin/console app:cron
 *
 * To output detailed information, increase the command verbosity:
 *
 *     $ php bin/console app:cron -vv
 *
 *
 * @package Tektine\Bundle\SchedulerBundle\Command
 */
class AppCronCommand extends Command
{
    /**
     * @var LoggerInterface
     */
    public $logHelper;

    /**
     * Cron can be launched ?
     *
     * @var boolean
     */
    protected $cronEnabled = false;

    /** @var EntityManagerInterface */
    private $entityManager;

    protected $options = [];

	/** @var bool  */
	private $force = false;

	/**
	 * @var NextRunComputer
	 */
	private $nextRunComputer;

	/**
	 * @var IMailHelper
	 */
	private $mailHelper;

	/** @var integer */
	private $jobId;
    /**
     * @var JobManager
     */
    private $jobManager;

    /**
     * AppCronCommand constructor.
     *
     * @param EntityManagerInterface $em
     * @param LoggerInterface $cronLogger
     * @param IGlobalParameter $parameter
     * @param NextRunComputer $nextRunComputer
     * @param IMailHelper $mailHelper
     */
	public function __construct(EntityManagerInterface $em,
                                LoggerInterface  $cronLogger,
                                IGlobalParameter $parameter,
                                NextRunComputer $nextRunComputer,
                                IMailHelper $mailHelper,
                                JobManager $jobManager)
	{
		parent::__construct();
		$this->logHelper       = $cronLogger;
		$this->entityManager   = $em;
		$this->cronEnabled     = $parameter->isCronTaskEnabled();
		$this->nextRunComputer = $nextRunComputer;
		$this->mailHelper      = $mailHelper;
        $this->jobManager = $jobManager;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:cron')
            ->setDescription('Lancement des crons applicatifs')
            ->addArgument('id', InputArgument::OPTIONAL, 'Numéro du cron à lancer')
	        ->addOption('force', null, InputOption::VALUE_NONE, 'Force cron execution')
        ;
    }

    /**
     * Lance la tache principale
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->jobId = $input->getArgument('id');
	    $this->force = $input->getOption('force');

        $this->logAction('Execution started');

        if ($this->cronEnabled !== true) {
            $this->logAction('Crons are disabled');
        } else {
            $jobs = $this->getScriptsToExecute();
            if (count($jobs) === 0) {
                $this->logAction('No crons to execute');
            }
            else {
                $this->logAction(sprintf('%s crons to execute', count($jobs)));
                $this->processJobs($jobs);
            }
        }
        $this->logAction('Execution ended');
        $output->writeln('Command result.');
        return 0;
    }

    /**
     * Retourne la liste des Jobs à executer
     */
    protected function getScriptsToExecute(): ?array
    {
    	/** @var SchedulerRepository $repo */
        $repo = $this->entityManager->getRepository(Scheduler::class);
	    if ($this->jobId)
	    {
		    $job = $repo->find($this->jobId);
		    if ($job instanceof Scheduler)
		    {
			    if ($this->force)
			    {
				    $this->logAction(sprintf("Lancement script Id, FORCE nextrun and unlocked : %s", $this->jobId));
				    // force nextrun and unlocked and enabled ?
				    $job->setNextRun(new \DateTime());
				    $job->setEnabled(true);
				    $job->setUnlocked(false);
				    $this->entityManager->flush();

				    return [$job];
			    }

                $this->logAction(sprintf("Lancement script Id, use nextrun and unlocked : %s", $this->jobId));

                return [$job];
            }
		    $this->logAction(sprintf("No script found for id %s", $this->jobId), LogLevel::ERROR);

		    return null;
	    }

        return $repo->getTasksToExecute();
    }

    /**
     * Lance les différentes tâches
     */
    protected function processJobs(array $jobs): void
    {
        foreach ($jobs as $job) {
            $currentJob = $job;
            try {
                $isLock = $this->isLockedAndMustBeExecuted($job);
                if ($isLock === false) {
                    $this->setLock($job, true);
                }

            } catch (\Exception $e) {
                $isLock = true;
                $this->logAction($e->__toString(), true, LogLevel::ERROR);
            }

            if ($isLock === false) {
                $this->processJob($job);

                try {
                    $this->setLock($job, false);
                }  catch (\Exception $e) {
                    $this->logAction(sprintf("Task %s could not be unlocked !\n%s", $job->getScriptClass(), $e), true, LogLevel::ERROR);
                }
            }
        }
    }

    /**
     * Exécute une tâche
     */
    protected function processJob(Scheduler $job): void
    {
        $now    = date('Y-m-d H:i:s');
        $action = $job->getScriptClass();
        $task   = $this->getTask($action);

        if(!$task) {
            $this->logAction(sprintf("Error : script %s will not be executed.", $action), true, LogLevel::ERROR);

            return;
        }

        try {
            $this->logAction(sprintf("Processing task %s", $action));
            $task->setArguments($job->getArguments());
            $task->configure($this->entityManager, $this->logHelper);
            $task->run();
            $this->logAction(sprintf("Execution succeeded with task %s", $action));
            $job->updateScheduler($now, false);
	        $job->setNextRun($this->computeNextRun($job, $task->hasAdditionalItemsToProcess()));
            $this->entityManager->flush();
        } catch (\Exception $e) {
            try {
                $this->sendAdminMailOnCronJobError($action, $e);
                $job->updateScheduler($now, true);
                $this->entityManager->flush();
                $this->logAction(sprintf("The following error occur while executing the script %s :\n%s", $action, $e->getMessage()), true, LogLevel::ERROR);
            } catch (\Exception $e2) {
                $this->logAction(sprintf("The following error occur while executing the script %s :\n%s", $action, $e->getMessage() . "\n" . $e2->getMessage()), true, LogLevel::ERROR);
            }
        }
    }

	protected function computeNextRun(Scheduler $job, $hasAdditionalItemsToProcess = false): ?\DateTime
    {
		if (!$job->getEnabled() || ($job->getExecutionCount() > 0 && $job->getExecutionCount() === $job->getMaxExecutionNumber())) {
            return null;
        }

		if ($job->getLastRun() === '') {
            return $job->getStartsOn();
        }

		if ($hasAdditionalItemsToProcess) {
            return new \DateTime();
        }

		return $this->nextRunComputer->computeNextRun($job->getExecutionRule(), $job->getStartsOn());
	}


	/**
     * Get crin task to execute
     *
     * @return JobBase | boolean
     */
    private function getTask(string $action)
    {
        $job = $this->jobManager->getJob($action);
        if ($job) {
            return $job;
        }
        $this->logAction(sprintf("No Job %s in JobManager", $action), LogLevel::WARNING);
        try {
            return new $action();
        } catch (\Exception $e) {
            $this->logAction(sprintf("The following error occur while building task to execute %s :\n%s", $action, $e->getMessage()), LogLevel::ERROR);
            return false;
        }
    }

    /**
     * Positionne le verrou sur la tâche en cours d'éxecution pour éviter un
     * empilement des exécutions.
     */
    private function setLock(Scheduler $job, bool $state, $setNextRun = false): void
    {
        try
        {
            $job->setUnlocked(!$state);
            if ($setNextRun) {
                $job->setNextRun($this->computeNextRun($job));
            }
            $this->entityManager->persist($job);
            $this->entityManager->flush();
        }
        catch (\Exception $e)
        {
            $msg = sprintf("The following error occured while setting lock to %d on task %s : %s", json_encode($state), $job->getScriptClass(), $e->getMessage());
            throw new \Exception($msg, $e->getCode(), $e);
        }
    }

    /**
     * Vérifie si la tâche est verrouillé
     */
    protected function isLockedAndMustBeExecuted(Scheduler $job, $now = 'now'): bool
    {
        $next = $job->getNextRun();
        $now  = new \DateTime($now);
        return !($job->getUnlocked() ==  true && $next <= $now);
    }

    /**
     * Log un message dans un fichier de logs spécifique.
     * Permet également un affichage sur STDOUT
     */
    protected function logAction(string $message, $stdout = true, $priority = LogLevel::INFO): void
    {
        $this->logHelper->log($priority, $message);
        if ($stdout) {
            printf("%s : %s\n", date('Y-m-d H:i:s'), $message);
        }
    }

    /**
     * Envoie un mail à production pour avertir du lock d'un cron
     */
    protected function sendAdminMailOnCronJobError(string $jobActionScriptName, \Exception $e): void
    {
	    $subject = "Erreur lors de l'execution d'une tache CRON : %s";
	    $body    = "Une erreur est survenue lors de l'execution d'une tache cron : <br /><br />
        	- Nom du script : " . $jobActionScriptName . "<br />
        	- Erreur retournée : " . $e->getMessage() . "<br />
        	- Trace : " . $e->getFile() . "<br />
        	- Trace : " . $e->getLine() . "<br />
        	- Trace : " . $e->getTraceAsString() . "<br />
        ";

	    $this->mailHelper->send($body, sprintf($subject, $jobActionScriptName));
    }

}
