<?php

declare(strict_types=1);

namespace Tektine\Bundle\SchedulerBundle\Job;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Tektine\Bundle\SchedulerBundle\Helper\HelperMemory;

/**
 * Classe mère pour toute job lancé par le script cron
 */
abstract class JobBase
{
    const BULK_SIZE = 500;

    /** @var LoggerInterface */
    protected $logger = null;

    /** @var \DateTime  */
    private $dateTime = null;

    /** @var boolean  */
    private $additionalItemsToProcess = false;

    /** @var HelperMemory */
    protected $helperMemory;

    /** @var  EntityManagerInterface $entityManager */
    protected $entityManager;

    /** @var string  */
    protected $arguments;

	/** @var string */
	protected $errorLog;

    /**
     * Méthode run à implémenter dans chaque classe fille.
     * Permet de lancer la tâche.
     */
    public abstract function run();

    /**
     * Méthode permettant de configurer la tâche.
     *  - Créé une instance de Log_Cron
     */
    public function configure(EntityManagerInterface $em, LoggerInterface $cronLogger)
    {
        $this->logger        = $cronLogger;
        $this->entityManager = $em;
        $this->helperMemory  = new HelperMemory();
        $this->dateTime      = new \DateTime();
    }

    /**
     * Log un message dans un fichier de logs spécifique.
     * Permet également un affichage sur STDOUT et dans une string si niveau ERROR
     * (pour envoi par mail par ex)
     * 
     * @param string  $message
     * @param boolean $stdout
     * @param integer $priority One of Log_Base::PRIORITY_*
     */
    protected function logAction($message, $stdout = true, $priority = LogLevel::INFO)
    {
        $message = '['.get_called_class().'] : '.$message;
        $this->logger->log($priority, $message);
	    $text = date('Y-m-d H:i:s') . ' [' . $priority . '] ' . $message ;
        if ($stdout)
            printf($text);
		if ($priority === LogLevel::ERROR) {
		    $this->errorLog .= $text;
	    }
    }

    /**
     * Log un message dans un fichier de logs spécifique.
     * Permet également un affichage sur STDOUT
     * Surcharge : affiche la consomation mémoire
     *
     * @param string  $message
     * @param boolean $stdout
     * @param integer $priority One of Log_Base::PRIORITY_*
     * @see Log_Base::logWithMemoryInformation
     */
    protected function logActionWithMemoryInfo($message, $stdout = true, $priority = LogLevel::INFO)
    {
        $message = $this->helperMemory->logActionWithMemoryInfo($message);
        $this->logAction($message, $stdout, $priority);
    }


    /**
     * Récupere la date du jour ou la date a laquelle le Cron doit être éxécuté
     *
     * @return \DateTime
     */
    public function getDateTime()
    {
       return $this->dateTime;
    }

    /**
     * Mettre à jour la date
     *
     * @param string $date
     * @throws \Exception valid date
     */
    public function setDateTime($date)
    {
        $this->dateTime = $date;
    }

    /**
     * Retourne la class en cours
     *
     * @return string
     */
    public function getClassName()
    {
        return get_called_class();
    }


    /**
     * Récupère le additionalItemsToProcess
     *
     * @return boolean
     */
    public function hasAdditionalItemsToProcess()
    {
        return $this->additionalItemsToProcess;
    }

    /**
     * Renseigne le has Additional Items To process
     *
     * @param boolean $value
     */
    protected function setHasAdditionalItemsToProcess($value)
    {
        $this->additionalItemsToProcess = $value;
    }


    /**
     * Set Helper Memory
     *
     * @param HelperMemory $helperMemory
     */
    public function setHelperMemory(HelperMemory $helperMemory)
    {
        $this->helperMemory = $helperMemory;
    }

    /**
     * GetHelperMemory
     *
     * @return HelperMemory
     */
    public function getHelperMemory()
    {
        return $this->helperMemory;
    }

    public function setArguments($arguments)
    {
        $this->arguments = $arguments;
    }

    public function getArguments()
    {
        return $this->arguments;
    }

    protected function bulkFlush($count = 0)
    {
        if (($count % self::BULK_SIZE) === 0) {
            $this->finalFlush();
        }
    }

    protected function finalFlush()
    {
        $this->logAction('flush ... ');
        $this->entityManager->flush();
        $this->logAction('end flush ... ');
    }
}