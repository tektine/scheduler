<?php

declare(strict_types=1);

namespace Tektine\Bundle\SchedulerBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 *
 * @author Nathalie Stanek
 */
class SchedulerRepository extends EntityRepository
{
    /**
     * Retourne une Doctrine_Collection de Scheduler qui doivent être exécutées
     * La méthode accepte une date particulière en parametre, sinon, la date du jour est utilisée
     * Les dates doivent être au format Y-m-d H:i:s
     *
     * @param mixed $date
     * @return array
     */
    public function getTasksToExecute($date = null)
    {
        if ($date == null)
            $date = date('Y-m-d H:i:s');
        $query = $this->createQueryBuilder('s')
            ->where('s.nextRun < :date')
            ->andWhere('s.nextRun IS NOT NULL')
            ->andWhere('s.enabled = :enabled')
            ->andWhere('s.unlocked = :unlocked')
            ->setParameters([
                'enabled' => true,
                'unlocked' => true,
                'date' => $date,
            ])
            ->getQuery();
        return $query->getResult();
    }


    /**
     * Récupère le scheduler avec le nom de class
     *
     * @param string $className
     * @return mixed
     */
    public function getSchedulerFromScriptClassName($className)
    {
        $query = $this->createQueryBuilder('s')
            ->innerJoin('s.JobAction', 'j')
            ->where('j.scriptClass=:scriptClass')
            ->setParameter('scriptClass', $className)
            ->setMaxResults(1)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

}
