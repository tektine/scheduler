<?php

declare(strict_types=1);

namespace Tektine\Bundle\SchedulerBundle\Model;

class GlobalParameter implements IGlobalParameter
{
	public function isCronTaskEnabled(): bool
	{
		return true;
	}
}