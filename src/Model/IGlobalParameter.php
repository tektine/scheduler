<?php

declare(strict_types=1);

namespace Tektine\Bundle\SchedulerBundle\Model;

interface IGlobalParameter
{
	public function isCronTaskEnabled(): bool;
}