<?php

namespace Tektine\Bundle\SchedulerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TektineSchedulerBundle extends Bundle
{
    public function getPath(): string
    {
        return \dirname(__DIR__);
    }
}
