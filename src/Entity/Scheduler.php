<?php

declare(strict_types=1);

namespace Tektine\Bundle\SchedulerBundle\Entity;

class Scheduler
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $jobName;

    /**
     * @var string
     */
    protected $scriptClass;

    /**
     * @var \DateTime
     */
    protected $nextRun;

    /**
     * @var \DateTime
     */
    protected $lastRun;

    /**
     * @var \DateTime
     */
    protected $lastFail;

	/**
	 * @var string
	 */
	protected $executionRule;

	/**
	 * @var string
	 */
	protected $timeout;

	/**
	 * @var string
	 * @deprecated 
	 */
	protected $repeats;

	/**
	 * @var integer
	 * @deprecated
	 */
	protected $repeatsEvery;

	/**
     * @var \DateTime
     */
    protected $startsOn;

    /**
     * @var integer
     */
    protected $executionCount = 1;

    /**
     * @var integer
     */
    protected $maxExecutionNumber = 0;

    /**
     * @var \DateTime
     */
    protected $endsOn;

    /**
     * @var string
     */
    protected $lastStatus;

    /**
     * @var boolean
     */
    protected $enabled;

    /**
     * @var boolean
     */
    protected $unlocked;

    /**
     * @var \DateTime
     */
    protected $referenceDate;

    /**
     * @var boolean
     */
    protected $isReferenceDateEditable;

	/**
	 * @var string
	 */
	protected $description;

    /**
     * @var array|null
     */
    protected $arguments;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLastStatus()
    {
        return $this->lastStatus;
    }

    /**
     * @param string $lastStatus
     */
    public function setLastStatus($lastStatus)
    {
        $this->lastStatus = $lastStatus;
    }

	public function getRepeats()
	{
		return $this->repeats;
	}
	public function setRepeats($ts)
	{
		$this->repeats = $ts;
		return $this;
	}

    public function getScriptClass()
    {
        return $this->scriptClass;
    }
    public function setScriptClass($script)
    {
    	$this->scriptClass = $script;
    }
    public function getEndsOn()
    {
        return $this->endsOn;
    }
	public function setEndsOn($ts)
	{
		$this->endsOn = $ts;
	}
    public function getStartsOn()
    {
        return $this->startsOn;
    }
	public function setStartsOn($ts)
	{
		$this->startsOn = $ts;
	}
	public function getRepeatsEvery()
	{
		return $this->repeatsEvery;
	}
	public function setRepeatsEvery($repeat)
	{
		$this->repeatsEvery = $repeat;
	}

    public function getUnlocked()
    {
        return $this->unlocked;
    }
    public function setUnlocked($bool)
    {
        $this->unlocked = $bool;
        return $this;
    }
    public function setEnabled($bool)
    {
    	$this->enabled = $bool;
    }
    public function getEnabled()
    {
    	return $this->enabled;
    }

	/**
	 * @return string
	 */
	public function getExecutionRule()
	{
		return $this->executionRule;
	}

	/**
	 * @param string $executionRule
	 */
	public function setExecutionRule( $executionRule)
	{
		$this->executionRule = $executionRule;
	}

	/**
	 * @return string
	 */
	public function getTimeout()
	{
		return $this->timeout;
	}

	/**
	 * @param string $timeout
	 */
	public function setTimeout( $timeout)
	{
		$this->timeout = $timeout;
	}

    public function getReferenceDate()
    {
        return$this->referenceDate;
    }

	/**
	 * @param \DateTime $referenceDate
	 */
	public function setReferenceDate($referenceDate)
	{
		$this->referenceDate = $referenceDate;
	}

	/**
	 * @param bool $isReferenceDateEditable
	 */
	public function setIsReferenceDateEditable($isReferenceDateEditable)
	{
		$this->isReferenceDateEditable = $isReferenceDateEditable;
	}

    public function getIsReferenceDateEditable()
    {
        return $this->isReferenceDateEditable;
    }
    public function getNextRun()
    {
        return $this->nextRun;
    }
    public function setNextRun($ts)
    {
        $this->nextRun = $ts;
        return $this;
    }
	public function getLastRun()
	{
		return $this->lastRun;
	}
	public function setLastRun($ts)
	{
		$this->lastRun = $ts;
		return $this;
	}
	public function setLastFail($ts)
	{
		$this->lastFail = $ts;
	}
	public function getLastFail()
	{
		return $this->lastFail;
	}
    public function getJobName()
    {
    	return $this->jobName;
    }
    public function setJobName($job)
    {
    	$this->jobName = $job;
    }
    public function getDescription()
    {
    	return $this->description;
    }
    public function setDescription($desc)
	{
		$this->description = $desc;
	}

    /**
     * @return int
     */
    public function getExecutionCount(): int
    {
        return $this->executionCount;
    }

    /**
     * @param int $executionCount
     */
    public function setExecutionCount(int $executionCount)
    {
        $this->executionCount = $executionCount;
    }

    /**
     * @return int
     */
    public function getMaxExecutionNumber(): int
    {
        return $this->maxExecutionNumber;
    }

    /**
     * @param int $maxExecutionNumber
     */
    public function setMaxExecutionNumber(int $maxExecutionNumber)
    {
        $this->maxExecutionNumber = $maxExecutionNumber;
    }

    /**
     * @return array|null
     */
    public function getArguments()
    {
        return $this->arguments;
    }

    /**
     * @param array $arguments
     */
    public function setArguments(?array $arguments)
    {
        $this->arguments = $arguments;
    }

	/**
	 * Met à jour les valeurs du Scheduler après une exécution
	 *
	 * @param string  $startDateTime
	 * @param boolean $hasError
	 *
	 * @return Scheduler
	 */
	public function updateScheduler($startDateTime, $hasError = false)
	{
		$this->setLastRun(\DateTime::createFromFormat('Y-m-d H:i:s', $startDateTime));

		if ($hasError) {
			$this->setLastStatus('error');
			$this->setLastFail($this->lastRun);
		} else {
			$this->setLastStatus('ok');
			$this->executionCount++;
		}
		return $this;
	}

	/**
	 * Check if the scheduler has the time Context
	 */
	public function hasTimeContext()
	{
		return $this->isReferenceDateEditable && $this->referenceDate != null;
	}

	/**
	 * Delete the time context
	 */
	public function resetTimeContext()
	{
		$this->referenceDate = null;
	}

}

